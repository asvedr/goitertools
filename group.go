package giter

func Group[T any, K comparable](
	get_key func(T) K,
	items ...T,
) map[K][]T {
	result := make(map[K][]T)
	for _, item := range items {
		key := get_key(item)
		arr, found := result[key]
		if found {
			result[key] = append(arr, item)
		} else {
			result[key] = []T{item}
		}
	}
	return result
}
