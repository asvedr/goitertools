package giter

func VToFlat[A any](vals ...[]A) []A {
	var result []A
	for _, val := range vals {
		result = append(result, val...)
	}
	return result
}

func V[A any](vals ...A) []A {
	return vals
}

func S[A comparable](vals ...A) map[A]struct{} {
	result := make(map[A]struct{})
	for _, val := range vals {
		result[val] = struct{}{}
	}
	return result
}

func V2M[A any, K comparable, V any](
	f func(A) (K, V), src ...A,
) map[K]V {
	result := make(map[K]V)
	for _, val := range src {
		k, v := f(val)
		result[k] = v
	}
	return result
}

func V2S[A comparable](src ...A) map[A]struct{} {
	result := make(map[A]struct{})
	for _, val := range src {
		result[val] = struct{}{}
	}
	return result
}

func M2V[K comparable, V any, A any](
	f func(K, V) A,
	src map[K]V,
) []A {
	var result []A
	for key, val := range src {
		result = append(result, f(key, val))
	}
	return result
}

func MKeys[K comparable, V any](src map[K]V) []K {
	keys := make([]K, 0, len(src))
	for k := range src {
		keys = append(keys, k)
	}
	return keys
}

func MValues[K comparable, V any](src map[K]V) []V {
	values := make([]V, 0, len(src))
	for _, v := range src {
		values = append(values, v)
	}
	return values
}

func M2M[K comparable, V any, K1 comparable, V1 any](
	f func(K, V) (K1, V1),
	src map[K]V,
) map[K1]V1 {
	result := make(map[K1]V1)
	for key, val := range src {
		key1, val1 := f(key, val)
		result[key1] = val1
	}
	return result
}

func MInvert[K comparable, V comparable](src map[K]V) map[V]K {
	return M2M(func(k K, v V) (V, K) { return v, k }, src)
}
