package giter_test

import (
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestMap(t *testing.T) {
	res := giter.Map(
		func(x int) int { return x + 1 },
		1, 2, 3, 4,
	)
	if !reflect.DeepEqual(res, []int{2, 3, 4, 5}) {
		t.Fatal(res)
	}
}

func TestMapErr(t *testing.T) {
	res, err := giter.MapErr(
		strconv.Atoi,
		"1", "2", "3", "4",
	)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(res, []int{1, 2, 3, 4}) {
		t.Fatal(res)
	}
	res, err = giter.MapErr(
		strconv.Atoi,
		"1", "2", "m", "4",
	)
	_, expected := strconv.Atoi("m")
	if err.Error() != expected.Error() {
		t.Fatalf("%v|%v", res, err)
	}
}
