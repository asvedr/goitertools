package giter_test

import (
	"reflect"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestGroup(t *testing.T) {
	f := func(r string) int { return len(r) }
	grouped := giter.Group(f,
		"abc",
		"def",
		"ghtc",
		"12345",
	)
	expected := map[int][]string{
		3: {"abc", "def"},
		4: {"ghtc"},
		5: {"12345"},
	}
	if !reflect.DeepEqual(grouped, expected) {
		t.Fatal(grouped)
	}
}
