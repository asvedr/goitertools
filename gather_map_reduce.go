package giter

func GatherMapReduce[T any, R any](
	f_map func(int, T) R,
	f_reduce func(R, R) R,
	acc R,
	src ...T,
) R {
	out := make(chan R)
	for i, val := range src {
		i_copy := i
		val_copy := val
		go func() { out <- f_map(i_copy, val_copy) }()
	}
	for i := 0; i < len(src); i += 1 {
		rval := <-out
		acc = f_reduce(acc, rval)
	}
	return acc
}

func GatherMapReduceErr[T any, R any](
	f_map func(int, T) (R, error),
	f_reduce func(R, R) (R, error),
	acc R,
	src ...T,
) (R, error) {
	out := make(chan R)
	err_ch := make(chan error)
	for i, val := range src {
		i_copy := i
		val_copy := val
		go func() {
			res, err := f_map(i_copy, val_copy)
			if err != nil {
				err_ch <- err
			}
			out <- res
		}()
	}
	var err error
	for i := 0; i < len(src); i += 1 {
		select {
		case err = <-err_ch:
			return acc, err
		case val := <-out:
			acc, err = f_reduce(acc, val)
		}
		if err != nil {
			return acc, err
		}
	}
	return acc, nil
}
