package giter

func FilterMap[A any, B any](
	f func(A) *B,
	slice ...A,
) []B {
	var result []B
	for _, val := range slice {
		mapped := f(val)
		if mapped != nil {
			result = append(result, *mapped)
		}
	}
	return result
}

func FilterMapErr[A any, B any](f func(A) (*B, error), slice ...A) ([]B, error) {
	var result []B
	for _, val := range slice {
		mapped, err := f(val)
		if err != nil {
			return nil, err
		}
		if mapped != nil {
			result = append(result, *mapped)
		}
	}
	return result, nil
}
