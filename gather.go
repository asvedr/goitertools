package giter

import "sync"

func Gather(funcs ...func()) {
	var wg sync.WaitGroup
	wg.Add(len(funcs))
	for _, f := range funcs {
		f_copy := f
		go func() { f_copy(); wg.Done() }()
	}
	wg.Wait()
}

func GatherErr(funcs ...func() error) error {
	err_ch := make(chan error)
	for _, f := range funcs {
		f_copy := f
		go func() { err_ch <- f_copy() }()
	}
	for i := 0; i < len(funcs); i += 1 {
		err := <-err_ch
		if err != nil {
			return err
		}
	}
	return nil
}

func GatherRes[T any](funcs ...func() T) []T {
	out := make(chan T)
	for _, f := range funcs {
		f_copy := f
		go func() { out <- f_copy() }()
	}
	var result []T
	for i := 0; i < len(funcs); i += 1 {
		result = append(result, <-out)
	}
	return result
}

func GatherResErr[T any](funcs ...func() (T, error)) ([]T, error) {
	out_ch := make(chan T)
	err_ch := make(chan error)
	for _, f := range funcs {
		f_copy := f
		go func() {
			val, err := f_copy()
			if err != nil {
				err_ch <- err
			} else {
				out_ch <- val
			}
		}()
	}
	var result []T
	for i := 0; i < len(funcs); i += 1 {
		select {
		case err := <-err_ch:
			return result, err
		case val := <-out_ch:
			result = append(result, val)
		}
	}
	return result, nil
}

func GatherMap[T any](f func(T), args ...T) {
	var wg sync.WaitGroup
	wg.Add(len(args))
	for _, arg := range args {
		arg_copy := arg
		go func() { f(arg_copy); wg.Done() }()
	}
	wg.Wait()
}

func GatherMapErr[T any](f func(T) error, args ...T) error {
	err_ch := make(chan error)
	for _, arg := range args {
		arg_copy := arg
		go func() { err_ch <- f(arg_copy) }()
	}
	for i := 0; i < len(args); i += 1 {
		err := <-err_ch
		if err != nil {
			return err
		}
	}
	return nil
}

func GatherMapRes[A any, T any](f func(A) T, args ...A) []T {
	out := make(chan T)
	for _, arg := range args {
		arg_copy := arg
		go func() { out <- f(arg_copy) }()
	}
	var result []T
	for i := 0; i < len(args); i += 1 {
		result = append(result, <-out)
	}
	return result
}

func GatherMapResErr[A any, T any](f func(A) (T, error), args ...A) ([]T, error) {
	out_ch := make(chan T)
	err_ch := make(chan error)
	for _, arg := range args {
		arg_copy := arg
		go func() {
			val, err := f(arg_copy)
			if err != nil {
				err_ch <- err
			} else {
				out_ch <- val
			}
		}()
	}
	var result []T
	for i := 0; i < len(args); i += 1 {
		select {
		case err := <-err_ch:
			return result, err
		case val := <-out_ch:
			result = append(result, val)
		}
	}
	return result, nil
}
