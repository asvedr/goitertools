package giter_test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestFilterMap(t *testing.T) {
	f := func(x int) *int {
		if x < 2 || x > 3 {
			return nil
		}
		x = x * 10
		return &x
	}
	res := giter.FilterMap(f, 1, 2, 3, 4)
	if !reflect.DeepEqual(res, []int{20, 30}) {
		t.Fatal(res)
	}
}

func TestFilterMapErr(t *testing.T) {
	f := func(x int) (*int, error) {
		if x < 0 {
			return nil, errors.New("oops")
		}
		if x < 2 || x > 3 {
			return nil, nil
		}
		x = x * 10
		return &x, nil
	}
	res, err := giter.FilterMapErr(f, 1, 2, 3, 4)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(res, []int{20, 30}) {
		t.Fatal(res)
	}
	res, err = giter.FilterMapErr(f, 1, 2, 3, -1)
	if err == nil || err.Error() != "oops" {
		t.Fatalf("%v|%v", res, err)
	}
}
