package giter_test

import (
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestSliceToMap(t *testing.T) {
	f := func(val string) (string, int) {
		split := strings.Split(val, " ")
		ival, _ := strconv.Atoi(split[1])
		return split[0], ival
	}
	res := giter.V2M(f, "a 1", "c 2", "e 3", "c 4")
	if !reflect.DeepEqual(
		res,
		map[string]int{"a": 1, "c": 4, "e": 3},
	) {
		t.Fatal(res)
	}
}

func TestSliceToSet(t *testing.T) {
	res := giter.V2S(1, 2, 3, 1)
	val := struct{}{}
	exp := map[int]struct{}{1: val, 2: val, 3: val}
	if !reflect.DeepEqual(res, exp) {
		t.Fatal(res)
	}
}

func TestMapToSlice(t *testing.T) {
	res := giter.M2V(
		func(i int, s string) string {
			return fmt.Sprintf("%d%s", i, s)
		},
		map[int]string{1: "a", 2: "b", 3: "c"},
	)
	ssl := sort.StringSlice(res)
	ssl.Sort()
	if !reflect.DeepEqual(
		[]string(ssl),
		[]string{"1a", "2b", "3c"},
	) {
		t.Fatal(res)
	}
}

func TestMapToMap(t *testing.T) {
	res := giter.MInvert(map[int]string{1: "a", 2: "b"})
	expected := map[string]int{"a": 1, "b": 2}
	if !reflect.DeepEqual(res, expected) {
		t.Fatal(res)
	}
}

func TestMKeys(t *testing.T) {
	res := giter.V2S(giter.MKeys(map[int]string{1: "a", 2: "b"})...)
	expected := giter.S(1, 2)
	if !reflect.DeepEqual(res, expected) {
		t.Fatal(res)
	}
}

func TestMVals(t *testing.T) {
	res := giter.V2S(giter.MValues(map[int]string{1: "a", 2: "b"})...)
	expected := giter.S("a", "b")
	if !reflect.DeepEqual(res, expected) {
		t.Fatal(res)
	}
}

func TestVToFlat(t *testing.T) {
	flat := giter.VToFlat([]int{1, 2, 3}, []int{4, 5, 6}, []int{7})
	if !reflect.DeepEqual(flat, []int{1, 2, 3, 4, 5, 6, 7}) {
		t.Fatal(flat)
	}
}
