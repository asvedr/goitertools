package giter_test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestFilter(t *testing.T) {
	f := func(x int) bool { return x%2 == 0 }
	res := giter.Filter(f, 1, 2, 3, 4)
	if !reflect.DeepEqual(res, []int{2, 4}) {
		t.Fatal(res)
	}
}

func TestFind(t *testing.T) {
	f := func(x string) bool { return x == "c" }
	if i := giter.Find(f, "a", "b", "c"); i != 2 {
		t.Fatal(i)
	}
	if i := giter.Find(f, "a", "b"); i != -1 {
		t.Fatal(i)
	}
}

func TestFilterErr(t *testing.T) {
	f := func(x int) (bool, error) {
		if x < 1 {
			return false, errors.New("neg")
		}
		return x%2 == 0, nil
	}

	res, err := giter.FilterErr(f, 1, 2, 3, 4)
	if err != nil || !reflect.DeepEqual(res, []int{2, 4}) {
		t.Fatalf("%v|%v", res, err)
	}

	res, err = giter.FilterErr(f, 1, 2, -1, 3, 4)
	if err.Error() != "neg" {
		t.Fatalf("%v|%v", res, err)
	}
}
