package giter_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/asvedr/giter"
)

func TestReduce(t *testing.T) {
	res := giter.Reduce(
		func(acc string, val int) string {
			return fmt.Sprintf("%s%d", acc, val)
		},
		"",
		1, 2, 3, 4,
	)
	if res != "1234" {
		t.Fatal(res)
	}
}

func TestReduceErr(t *testing.T) {
	f := func(acc string, val int) (string, error) {
		if val > 2 {
			return "", errors.New("oops")
		}
		return fmt.Sprintf("%s%d", acc, val), nil
	}
	res, err := giter.ReduceErr(f, "", 1, 2, 3, 4)
	if err.Error() != "oops" {
		t.Fatalf("%v|%v", res, err)
	}
	res, err = giter.ReduceErr(f, "", 1, 2, 2, 2)
	if res != "1222" || err != nil {
		t.Fatalf("%v|%v", res, err)
	}
}
