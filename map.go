package giter

func Map[A any, B any](f func(A) B, slice ...A) []B {
	var result []B
	for _, val := range slice {
		result = append(result, f(val))
	}
	return result
}

func MapErr[A any, B any](f func(A) (B, error), slice ...A) ([]B, error) {
	var result []B
	for _, val := range slice {
		mapped, err := f(val)
		if err != nil {
			return nil, err
		}
		result = append(result, mapped)
	}
	return result, nil
}
