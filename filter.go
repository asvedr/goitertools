package giter

func Filter[A any](f func(A) bool, slice ...A) []A {
	var result []A
	for _, val := range slice {
		if f(val) {
			result = append(result, val)
		}
	}
	return result
}

func FilterErr[A any](f func(A) (bool, error), slice ...A) ([]A, error) {
	var result []A
	for _, val := range slice {
		keep, err := f(val)
		if err != nil {
			return nil, err
		}
		if keep {
			result = append(result, val)
		}
	}
	return result, nil
}

func Find[A any](f func(A) bool, slice ...A) int {
	for i, val := range slice {
		if f(val) {
			return i
		}
	}
	return -1
}
