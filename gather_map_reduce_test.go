package giter_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"gitlab.com/asvedr/giter"
)

func TestMapReduce(t *testing.T) {
	f_map := func(i, val int) string {
		ms := time.Duration(i * 100)
		time.Sleep(ms * time.Millisecond)
		return fmt.Sprintf("%d%d", i, val)
	}
	f_red := func(a, b string) string { return a + b }
	res := giter.GatherMapReduce(f_map, f_red, "", 7, 8, 9)
	if res != "071829" {
		t.Fatal(res)
	}
}

func TestMapReduceErr(t *testing.T) {
	f_map := func(i, val int) (string, error) {
		if val < 0 {
			return "", errors.New("map")
		}
		ms := time.Duration(i * 100)
		time.Sleep(ms * time.Millisecond)
		return fmt.Sprintf("%d%d", i, val), nil
	}
	f_red := func(a, b string) (string, error) {
		if b == "11" {
			return "", errors.New("red")
		}
		return a + b, nil
	}
	res, err := giter.GatherMapReduceErr(f_map, f_red, "", 7, 8, 9)
	if err != nil {
		t.Fatal(err)
	}
	if res != "071829" {
		t.Fatal(res)
	}

	res, err = giter.GatherMapReduceErr(f_map, f_red, "", -1, 2)
	if err == nil || err.Error() != "map" {
		t.Fatal(err)
	}
	res, err = giter.GatherMapReduceErr(f_map, f_red, "", 2, 1)
	if err == nil || err.Error() != "red" {
		t.Fatal(err)
	}
}
