package giter_test

import (
	"errors"
	"reflect"
	"sync"
	"testing"
	"time"

	"gitlab.com/asvedr/giter"
)

type Arg struct {
	ms  int
	msg int
}

func TestGather(t *testing.T) {
	var m sync.Mutex
	var out []int
	f := func(ms int, msg int) func() {
		return func() {
			time.Sleep(time.Millisecond * time.Duration(ms))
			m.Lock()
			out = append(out, msg)
			m.Unlock()
		}
	}
	begin := time.Now()
	giter.Gather(f(100, 2), f(300, 3), f(600, 4), f(900, 5))
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if !reflect.DeepEqual(out, []int{2, 3, 4, 5}) {
		t.Fatal(out)
	}
}

func TestGatherMap(t *testing.T) {
	var m sync.Mutex
	var out []int

	f := func(arg Arg) {
		time.Sleep(time.Millisecond * time.Duration(arg.ms))
		m.Lock()
		out = append(out, arg.msg)
		m.Unlock()
	}
	begin := time.Now()
	giter.GatherMap(
		f,
		Arg{ms: 100, msg: 2},
		Arg{ms: 300, msg: 3},
		Arg{ms: 600, msg: 4},
		Arg{ms: 900, msg: 5},
	)
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if !reflect.DeepEqual(out, []int{2, 3, 4, 5}) {
		t.Fatal(out)
	}
}

func TestGatherErr(t *testing.T) {
	var m sync.Mutex
	var out []int
	f := func(ms int, msg int) func() error {
		return func() error {
			if msg < 0 {
				return errors.New("oops")
			}
			time.Sleep(time.Millisecond * time.Duration(ms))
			m.Lock()
			out = append(out, msg)
			m.Unlock()
			return nil
		}
	}
	begin := time.Now()
	err := giter.GatherErr(f(700, 2), f(900, 3))
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(out, []int{2, 3}) {
		t.Fatal(out)
	}
}

func TestGatherMapErr(t *testing.T) {
	var m sync.Mutex
	var out []int
	f := func(arg Arg) error {
		if arg.msg < 0 {
			return errors.New("oops")
		}
		time.Sleep(time.Millisecond * time.Duration(arg.ms))
		m.Lock()
		out = append(out, arg.msg)
		m.Unlock()
		return nil
	}
	begin := time.Now()
	err := giter.GatherMapErr(f, Arg{ms: 700, msg: 2}, Arg{ms: 900, msg: 3})
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(out, []int{2, 3}) {
		t.Fatal(out)
	}
}

func TestGatherRes(t *testing.T) {
	f := func(ms int, res int) func() int {
		return func() int {
			time.Sleep(time.Millisecond * time.Duration(ms))
			return res
		}
	}
	begin := time.Now()
	res := giter.GatherRes(f(100, 2), f(300, 3), f(600, 4), f(900, 5))
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if !reflect.DeepEqual(res, []int{2, 3, 4, 5}) {
		t.Fatal(res)
	}
}

func TestGatherMapRes(t *testing.T) {
	f := func(arg Arg) int {
		time.Sleep(time.Millisecond * time.Duration(arg.ms))
		return arg.msg
	}
	begin := time.Now()
	res := giter.GatherMapRes(
		f,
		Arg{ms: 100, msg: 2},
		Arg{ms: 300, msg: 3},
		Arg{ms: 600, msg: 4},
		Arg{ms: 900, msg: 5},
	)
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if !reflect.DeepEqual(res, []int{2, 3, 4, 5}) {
		t.Fatal(res)
	}
}

func TestGatherResErr(t *testing.T) {
	f := func(ms int, res int) func() (int, error) {
		return func() (int, error) {
			if res < 0 {
				return 0, errors.New("oops")
			}
			time.Sleep(time.Millisecond * time.Duration(ms))
			return res, nil
		}
	}
	begin := time.Now()
	res, err := giter.GatherResErr(f(100, 2), f(300, 3), f(600, 4), f(900, 5))
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(res, []int{2, 3, 4, 5}) {
		t.Fatal(res)
	}
	res, err = giter.GatherResErr(f(100, 2), f(300, -1))
	if err == nil || err.Error() != "oops" {
		t.Fatal(err)
	}
}

func TestGatherMapResErr(t *testing.T) {
	f := func(arg Arg) (int, error) {
		if arg.msg < 0 {
			return 0, errors.New("oops")
		}
		time.Sleep(time.Millisecond * time.Duration(arg.ms))
		return arg.msg, nil
	}
	begin := time.Now()
	res, err := giter.GatherMapResErr(
		f,
		Arg{ms: 100, msg: 2},
		Arg{ms: 300, msg: 3},
		Arg{ms: 600, msg: 4},
		Arg{ms: 900, msg: 5},
	)
	end := time.Now()
	if end.Sub(begin).Seconds() > 1.0 {
		t.Fatalf("wait too long: %v", end.Sub(begin).Seconds())
	}
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(res, []int{2, 3, 4, 5}) {
		t.Fatal(res)
	}
	res, err = giter.GatherMapResErr(
		f, Arg{ms: 100, msg: 2}, Arg{ms: 300, msg: -1},
	)
	if err == nil || err.Error() != "oops" {
		t.Fatal(err)
	}
}
