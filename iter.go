package giter

func IterErr(funs ...func() error) error {
	for _, f := range funs {
		if err := f(); err != nil {
			return err
		}
	}
	return nil
}

func FAsg[T any](dst *T, fsrc func() (T, error)) func() error {
	return func() error {
		val, err := fsrc()
		*dst = val
		return err
	}
}
