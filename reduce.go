package giter

func Reduce[T any, R any](
	f func(R, T) R,
	acc R,
	src ...T,
) R {
	for _, val := range src {
		acc = f(acc, val)
	}
	return acc
}

func ReduceErr[T any, R any](
	f func(R, T) (R, error),
	acc R,
	src ...T,
) (R, error) {
	var err error
	for _, val := range src {
		acc, err = f(acc, val)
		if err != nil {
			return acc, err
		}
	}
	return acc, nil
}
